AYUDANTÍAS DE JAVASCRIPT - GRUPO 3
---
Reforzamiento de conceptos de JavaScript.

### Obtener
```
git clone --recursive git@gitlab.com:desafiolatam-react-g3/teaching-assistantships-javascript.git
```

### Contenido
- [Primera clase](https://gitlab.com/desafiolatam-react-g3/1.javascript-part-one)
    + EcmaScript 2015
    + Eventos
    + Objetos literales
- [Segunda clase](https://gitlab.com/desafiolatam-react-g3/2.javascript-part-two)
    + Clases y objetos
    + Métodos map, filter y reduce
    + Funciones

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
